#!/bin/sh

dir="${0%/*}"

cp $dir/src/android/* platforms/android/src/org/jetteam/cordova/
cp $dir/www/* platforms/android/build/intermediates/assets/debug/www/plugins/cordova-plugin-jetteam/www/
cp $dir/www/* platforms/android/assets/www/plugins/cordova-plugin-jetteam/www/
# cp $dir/www/* platforms/android/platform_www/plugins/cordova-plugin-jetteam/www/
