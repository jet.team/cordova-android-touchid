package org.jetteam.cordova;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.util.Base64;

public class KeyStoreHelper {
    private static final String ANDROID_KEY_STORE = "AndroidKeyStore";

    private KeyStore mKeyStore = null;
    private KeyGenerator mKeyGenerator = null;
    private final boolean mUserAuthenticationRequired;

    KeyStoreHelper(final boolean userAuthenticationRequired) {
        mUserAuthenticationRequired = userAuthenticationRequired;

        try {
            mKeyGenerator = KeyGenerator.getInstance("AES", ANDROID_KEY_STORE);
            mKeyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
            mKeyStore.load(null);
        } catch (CertificateException | NoSuchAlgorithmException | NoSuchProviderException | KeyStoreException | IOException e) {
            e.printStackTrace();
        }
    }

    private Cipher createCipher() {
        Cipher cipher = null;
        try {
           cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return cipher;
    }

    private Key getKey(String keyStoreKey) {
        Key key = null;
        try {
            key = mKeyStore.getKey(keyStoreKey, null);
        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
            e.printStackTrace();
        }
        return key;
    }

    public boolean hasKey(String keyStoreKey) {
        try {
            return mKeyStore.containsAlias(keyStoreKey);
        } catch(KeyStoreException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean removeKey(String keyStoreKey) {
        try {
            mKeyStore.deleteEntry(keyStoreKey);
            return true;
        } catch(KeyStoreException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean initCipher(Cipher cipher, int mode, Key key, String keyStoreKey, String IV) {
        try {
            if (IV == null) {
                cipher.init(mode, key);
            } else {
                byte[] ivBytes = Base64.decode(IV, Base64.NO_WRAP);
                cipher.init(mode, key, new IvParameterSpec(ivBytes));
            }
        } catch(KeyPermanentlyInvalidatedException e) {
            try {
                mKeyStore.deleteEntry(keyStoreKey);
            } catch(KeyStoreException er) {
                er.printStackTrace();
            }
        } catch(InvalidKeyException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Cipher createDecryptCipher(String keyStoreKey, String IV) {
        Cipher cipher = createCipher();
        if (cipher == null) return null;
        Key key = getKey(keyStoreKey);
        if (key == null) return null;
        initCipher(cipher, Cipher.DECRYPT_MODE, key, keyStoreKey, IV);
        return cipher;
    }

    public Cipher createEncryptCipher(String keyStoreKey) {
        Cipher cipher = createCipher();
        if (cipher == null) return null;
        Key key = getKey(keyStoreKey);
        if (key == null) {
            key = createKey(keyStoreKey);
        }
        if (key == null) return null;
        initCipher(cipher, Cipher.ENCRYPT_MODE, key, keyStoreKey, null);
        return cipher;
    }

    public static String getCipherIV(Cipher cipher) {
        return Base64.encodeToString(cipher.getIV(), Base64.NO_WRAP);
    }

    private Key createKey(String keyStoreKey) {
        Key key = null;
        try {
            if (mUserAuthenticationRequired || android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                mKeyGenerator.init(new KeyGenParameterSpec.Builder(keyStoreKey,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT).setBlockModes(
                    KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(mUserAuthenticationRequired)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            } else {
                mKeyGenerator.init(256);
            }
            key = mKeyGenerator.generateKey();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return key;
    }

    public static String decrypt(String data, Cipher cipher) {
        if (cipher == null) return null;
        try {
            byte[] bytes = Base64.decode(data, Base64.NO_WRAP);
            return new String(cipher.doFinal(bytes));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String encrypt(String data, Cipher cipher) {
        if (cipher == null) return null;
        try {
            byte[] bytes = cipher.doFinal(data.getBytes());
            return Base64.encodeToString(bytes, Base64.NO_WRAP);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }
}