package org.jetteam.cordova;

import javax.crypto.Cipher;

import android.app.KeyguardManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.os.CancellationSignal;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;

public class FingerprintHelper extends FingerprintManagerCompat.AuthenticationCallback {
    static FingerprintHelper instance = null;

    public enum SensorState {
        NOT_SUPPORTED, 
        NOT_BLOCKED, // если устройство не защищено пином, рисунком или паролем
        NO_FINGERPRINTS, // если на устройстве нет отпечатков
        READY 
    }

    public interface ListenCallback {
        void message(int msgId, String msg);
        void complete(boolean success);
    }

    private final Context mContext;
    private final FingerprintManagerCompat mFingerprintManager;
    private CancellationSignal mCancellationSignal = null;
    private ListenCallback mListenCallback;
    
    public final SensorState mSensorState;

    static FingerprintHelper getInstance(@NonNull Context context) {
        if (instance == null) {
            instance = new FingerprintHelper(context);
        }
        return instance;
    }

    static void stopLastInstance() {
        if (instance != null) {
            instance.stop();
            instance = null;
        }
    }

    private FingerprintHelper(@NonNull Context context) {
        mContext = context;
        mFingerprintManager = FingerprintManagerCompat.from(context);
        mSensorState = getSensorState();
    }

    public SensorState getSensorState() {
        if (mFingerprintManager.isHardwareDetected()) {
            KeyguardManager keyguardManager = (KeyguardManager) mContext.getSystemService(Context.KEYGUARD_SERVICE);
            if (!keyguardManager.isKeyguardSecure()) {
                return SensorState.NOT_BLOCKED;
            }

            if (!mFingerprintManager.hasEnrolledFingerprints()) {
                return SensorState.NO_FINGERPRINTS;
            }
            return SensorState.READY;
        } else {
            return SensorState.NOT_SUPPORTED;
        }
    }

    public boolean listen(Cipher cipher, ListenCallback callback) {
        if (mSensorState != SensorState.READY) return false;
        mListenCallback = callback;
        FingerprintManagerCompat.CryptoObject cryptoObject = new FingerprintManagerCompat.CryptoObject(cipher);
        mCancellationSignal = new CancellationSignal();
        mFingerprintManager.authenticate(cryptoObject, 0, mCancellationSignal, this, null);
        return true;
    }

    public void stop() {
        if (mCancellationSignal != null) {
            mCancellationSignal.cancel();
            mCancellationSignal = null;
        }
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        mListenCallback.message(errMsgId, errString.toString());
    }

    @Override
    public void onAuthenticationFailed() {
        mListenCallback.complete(false);
        instance = null;
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        mListenCallback.message(helpMsgId, helpString.toString());
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
        mListenCallback.complete(true);
        instance = null;
    }
}