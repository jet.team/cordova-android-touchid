
package org.jetteam.cordova;

import javax.crypto.Cipher;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.util.Base64;
import android.content.Context;
import android.content.SharedPreferences;

/**
* This class exposes methods in Cordova that can be called from JavaScript.
*/
public class JetTeamPlugin extends CordovaPlugin {
    private static final String SHARED_PREFS_NAME = "JetTeamCordova";
    private static final String ENCRYPTED_SUFFIX = "_encrypted";
    private static final String IV_SUFFIX = "_IV";

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArry of arguments for the plugin.
     * @param callbackContext   The callback context from which we were invoked.
     */
    @SuppressLint("NewApi")
    public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if (action.equals("fingerprint_set")) {
            final String key = args.getString(0);
            final String data = args.getString(1);
            final Context context = cordova.getActivity().getApplicationContext();
            final FingerprintHelper fingerprint = FingerprintHelper.getInstance(context);
            if (fingerprint.mSensorState != FingerprintHelper.SensorState.READY) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, 415));
                return true;
            }
            final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
            final KeyStoreHelper keyStore = new KeyStoreHelper(true);

            final Cipher cipher = keyStore.createEncryptCipher(key);
            fingerprint.listen(cipher, new FingerprintHelper.ListenCallback(){

                @Override
                public void message(int msgId, String msg) {}
            
                @Override
                public void complete(boolean success) {
                    PluginResult result;
                    if (success) {
                        String encrypted = KeyStoreHelper.encrypt(data, cipher);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(key + ENCRYPTED_SUFFIX, encrypted);
                        editor.putString(key + IV_SUFFIX, KeyStoreHelper.getCipherIV(cipher));
                        editor.apply();
                        result = new PluginResult(PluginResult.Status.OK);
                    } else {
                        result = new PluginResult(PluginResult.Status.ERROR, 403);
                    }
                    callbackContext.sendPluginResult(result);
                }
            });
        } else if(action.equals("fingerprint_get")) {
            final String key = args.getString(0);
            final Context context = cordova.getActivity().getApplicationContext();
            final FingerprintHelper fingerprint = FingerprintHelper.getInstance(context);
            if (fingerprint.mSensorState != FingerprintHelper.SensorState.READY) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, 415));
                return true;
            }
            final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
            final KeyStoreHelper keyStore = new KeyStoreHelper(true);
            if (sharedPreferences.getString(key + ENCRYPTED_SUFFIX, null) == null || !keyStore.hasKey(key)) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, 404));
                return true;
            }

            final Cipher cipher = keyStore.createDecryptCipher(key, sharedPreferences.getString(key + IV_SUFFIX, null));
            fingerprint.listen(cipher, new FingerprintHelper.ListenCallback(){
            
                @Override
                public void message(int msgId, String msg) {}

                @Override
                public void complete(boolean success) {
                    PluginResult result;
                    if (success) {
                        String encrypted = sharedPreferences.getString(key + ENCRYPTED_SUFFIX, null);
                        String decrypted = KeyStoreHelper.decrypt(encrypted, cipher);
                        result = new PluginResult(PluginResult.Status.OK, decrypted);
                    } else {
                        result = new PluginResult(PluginResult.Status.ERROR, 403);
                    }
                    callbackContext.sendPluginResult(result);
                }
            });
        } else if(action.equals("fingerprint_verify")) {
            final String key = args.getString(0);
            final Context context = cordova.getActivity().getApplicationContext();
            final FingerprintHelper fingerprint = FingerprintHelper.getInstance(context);
            if (fingerprint.mSensorState != FingerprintHelper.SensorState.READY) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, 415));
                return true;
            }
            final KeyStoreHelper keyStore = new KeyStoreHelper(true);

            final Cipher cipher = keyStore.createEncryptCipher(key);
            fingerprint.listen(cipher, new FingerprintHelper.ListenCallback(){
            
                @Override
                public void message(int msgId, String msg) {}

                @Override
                public void complete(boolean success) {
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, success));
                }
            });
        } else if(action.equals("fingerprint_stop")) {
            FingerprintHelper.stopLastInstance();
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
        } else if(action.equals("fingerprint_status")) {
            final Context context = cordova.getActivity().getApplicationContext();
            final FingerprintHelper fingerprint = FingerprintHelper.getInstance(context);
            String status;
            switch (fingerprint.mSensorState) {
                case NOT_SUPPORTED:
                    status = "NotSupported";
                    break;
                case NOT_BLOCKED:
                    status = "NotBlocked";
                    break;
                case NO_FINGERPRINTS:
                    status = "NoFingerprint";
                    break;
                case READY:
                    status = "Ready";
                    break;
                default:
                    status = "SomeNewEnumValue";
            }
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, status));
        } else if(action.equals("keystore_get")) {
            final String key = args.getString(0);
            final Context context = cordova.getActivity().getApplicationContext();
            final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
            final KeyStoreHelper keyStore = new KeyStoreHelper(true);
            if (sharedPreferences.getString(key + ENCRYPTED_SUFFIX, null) == null || !keyStore.hasKey(key)) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, 404));
                return true;
            }

            final Cipher cipher = keyStore.createDecryptCipher(key, sharedPreferences.getString(key + IV_SUFFIX, null));

            String encrypted = sharedPreferences.getString(key + ENCRYPTED_SUFFIX, null);
            String decrypted = KeyStoreHelper.decrypt(encrypted, cipher);
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, decrypted));
        } else if(action.equals("keystore_set")) {
            final String key = args.getString(0);
            final String data = args.getString(1);
            final Context context = cordova.getActivity().getApplicationContext();
            final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
            final KeyStoreHelper keyStore = new KeyStoreHelper(false);

            final Cipher cipher = keyStore.createEncryptCipher(key);

            String encrypted = KeyStoreHelper.encrypt(data, cipher);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key + ENCRYPTED_SUFFIX, encrypted);
            editor.putString(key + IV_SUFFIX, KeyStoreHelper.getCipherIV(cipher));
            editor.apply();
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
        } else if(action.equals("keystore_remove")) {
            final String key = args.getString(0);
            final Context context = cordova.getActivity().getApplicationContext();
            final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
            final KeyStoreHelper keyStore = new KeyStoreHelper(false);

            keyStore.removeKey(key);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(key + ENCRYPTED_SUFFIX);
            editor.remove(key + IV_SUFFIX);
            editor.apply();
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
        } else if(action.equals("keystore_has")) {
            final String key = args.getString(0);
            final Context context = cordova.getActivity().getApplicationContext();
            final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
            
            boolean has = sharedPreferences.getString(key + ENCRYPTED_SUFFIX, null) != null;
            boolean hasIv = sharedPreferences.getString(key + IV_SUFFIX, null) != null;
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, has && hasIv));
        } else {
            return false;
        }
        return true;
    }
}
