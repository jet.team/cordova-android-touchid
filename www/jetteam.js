/* global cordova:false */
/* globals window */

var exec = cordova.require('cordova/exec'),
    utils = cordova.require('cordova/utils');

var template = {
    fingerprint_status: function(successCallback, errorCallback) {
        exec(successCallback, errorCallback, 'JetTeamPlugin', 'fingerprint_status');
    },

    fingerprint_set: function(successCallback, errorCallback, key, value) {
        exec(successCallback, errorCallback, 'JetTeamPlugin', 'fingerprint_set', [key, value]);
    },

    fingerprint_get: function(successCallback, errorCallback, key) {
        exec(successCallback, errorCallback, 'JetTeamPlugin', 'fingerprint_get', [key]);
    },

    fingerprint_verify: function(successCallback, errorCallback, key) {
        exec(successCallback, errorCallback, 'JetTeamPlugin', 'fingerprint_verify', [key]);
    },

    fingerprint_stop: function() {
        exec(function(){}, function(){}, 'JetTeamPlugin', 'fingerprint_stop');
    },

    keystore_set: function(successCallback, errorCallback, key, value) {
        exec(successCallback, errorCallback, 'JetTeamPlugin', 'keystore_set', [key, value]);
    },

    keystore_get: function(successCallback, errorCallback, key) {
        exec(successCallback, errorCallback, 'JetTeamPlugin', 'keystore_get', [key]);
    },

    keystore_remove: function(successCallback, errorCallback, key) {
        exec(successCallback, errorCallback, 'JetTeamPlugin', 'keystore_remove', [key]);
    },

    keystore_has: function(successCallback, key) {
        exec(successCallback, function(){}, 'JetTeamPlugin', 'keystore_has', [key]);
    }
};

module.exports = template;
